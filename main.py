# -*- coding: utf-8 -*-	
from tkinter import * 
from tkinter import messagebox
import serial, time
import serial.tools.list_ports

root = Tk()
root.title('Calculadora binario') 
root.geometry('296x370') 
root.config(bg='#272822')
root.iconbitmap('icon.ico')
root.resizable(0,0)

def errores(errorString):
	messagebox.showerror("Error", errorString)

global serialPort
serialPort = ""
try:
	ports = list(serial.tools.list_ports.comports())
	for p in ports:
		aux = p
		serialPort = str(aux)[:4]
	ser = serial.Serial(serialPort, 9600, timeout=0)
except serial.SerialException as e:
	root.withdraw()
	errores("Verifique la conexión del Arduino.")
	root.destroy()

global screenVar
global_string = ""
global allString
allString = ""
global cont_
cont_ = int(0)
global opeType
opeType = ""
global boolean
boolean = False
global var1, var2

##ACCIÓN DE BOTONES '0, 1, 2, 3, 4, 5, 6, 7, 8, 9'
def functEntry(variable):
	if(cont_ < 3):
		global global_string
		global cont_
		cont_ += 1
		global_string += "" + variable
		screenEntry.config(state="normal")
		actual_count = len(global_string)
		screenEntry.delete(0, END)
		screenEntry.insert(0, global_string)
		screenEntry.config(state="disable")
		global allString
		allString += "" + variable
		screenAll.config(state="normal")
		screenAll.delete(0, END)
		screenAll.insert(0, allString)
		screenAll.config(state="disable")
	else:
		errores("Se ha sobrepasado el límite de carácteres permitidos (3).")

##ACCIÓN DE BOTÓN 'C'
def deleteEntry():
	if(boolean == False):
		if(cont_ > 0):
			global global_string
			global allString
			global cont_
			cont_ -= 1
			actual_count = len(global_string)
			screenEntry.config(state="normal")
			screenEntry.delete(actual_count-1, END)
			screenEntry.config(state="disable")
			global_string = global_string[:-1]
			
			actual_count2 = len(allString)
			screenAll.config(state="normal")
			screenAll.delete(actual_count2-1, END)
			screenAll.config(state="disable")
			allString = allString[:-1]

		else:
			errores("No hay número para borrar.")
	else:
		global var1, var2, global_string, cont_, opeType, boolean, allString, screenAll
		var1 = ""
		var2 = ""
		global_string = ""
		allString = ""
		opeType = ""
		cont_ = 0
		boolean = False
		screenEntry.config(state="normal")
		screenEntry.delete(0, END)
		screenEntry.config(state="disable")
		screenAll.config(state="normal")
		screenAll.delete(0, END)
		screenAll.config(state="disable")

##ACCIÓN DE BOTONES '+, -, ×, ÷'
def oper(oper):
	global opeType, global_string, var1, boolean, cont_, allString
	if(boolean==False and len(global_string)!=0):
		opeType = oper
		##boolean = True
		var1 = global_string
		global_string = ""
		cont_ = 0
		screenEntry.config(state="normal")
		screenEntry.delete(0, END)
		screenEntry.config(state="disable")

		allString += " " + oper + " "
		screenAll.config(state="normal")
		screenAll.delete(0, END)
		screenAll.insert(0, allString)
		screenAll.config(state="disable")

##FUNCIÓN PARA MANDAR DATOS AL ARDUINO
def sendInfo(var1, var2, opeType):
	operAux = opeType
	if(len(str(var1)) == 1):
		var1 = "00" + str(var1)
	if(len(str(var1)) == 2):
		var1 = "0" + str(var1)
	if(len(str(var2)) == 1):
		var2 = "00" + str(var2)
	if(len(str(var2)) == 2):
		var2 = "0" + str(var2)
	if(operAux == "×"):
		operAux = "x"
	if(operAux == "÷"):
		operAux = "/"
	stringAux = str(var1)+str(operAux)+str(var2)
	ser.write(stringAux.encode()) 
	time.sleep(1)
	print (str(ser.readline().decode()))

##FUNCIÓN AUXILIAR DE LA FUNCIÓN 'Iqual()'
def resultFunct(opeType, var1, var2):
	if(opeType == "+"):
		return int(var1)+int(var2)
	if(opeType == "-"):
		return int(var1)-int(var2)
	if(opeType == "×"):
		return int(var1)*int(var2)
	if(opeType == "÷"):
		return int(var1)/int(var2)

##ACCIÓN DE BOTÓN '='
def Iqual():
	try:
		global opeType, global_string, var1, var2,boolean, cont_
		if(boolean==False and len(global_string)!=0):
			boolean = True
			var2 = global_string
			global_string = ""
			cont_ = 0
			aux = int(resultFunct(opeType, int(var1), int(var2)))
			if(aux<256 and aux>-1):
				screenEntry.config(state="normal")
				screenEntry.delete(0, END)
				screenEntry.insert(0, aux)
				screenEntry.config(state="disable")
				screenAll.config(state="normal")
				screenAll.delete(0, END)
				screenAll.insert(0, allString+ " = " +str(aux) + " (" + str(bin(aux).lstrip("0b")) + ")")
				screenAll.config(state="disable")
				sendInfo(var1, var2, opeType)
			else:
				errores("El resultado debe estar en un rango entre 0 y 255 para su representación en binario (8 bits).")
	except serial.SerialException as e:
		errores("Se ha perdido la conexión con el Arduino.")

##DECLARACIÓN DE ENTRY EN TIEMPO REAL
screenAll = Entry(root, bd=0, width=27, textvariable=allString, font=('Verdana', 8), justify='right', state="disable")
screenAll.grid()
screenAll.place(x=96, y=5)
screenAll.config(disabledbackground="#272822", disabledforeground="#fff")
##DECLARACIÓN DE COMPONENTES GRÁFICOS
screenVar = StringVar()
screenEntry = Entry(root, bd=1, width=16, textvariable=screenVar, font=('Verdana', 20), justify='right', state="disable")
screenEntry.grid()
screenEntry.place(x=10, y=25)
screenEntry.config(disabledbackground="#FFF", disabledforeground="#000")

btn7 = Button(root, text="7", width=3 ,font=('Verdana', 20), command=lambda: functEntry("7")).place(x=10, y=90)
btn8 = Button(root, text="8", width=3 ,font=('Verdana', 20), command=lambda: functEntry("8")).place(x=80, y=90)
btn9 = Button(root, text="9", width=3 ,font=('Verdana', 20), command=lambda: functEntry("9")).place(x=150, y=90)

btn4 = Button(root, text="4", width=3 ,font=('Verdana', 20), command=lambda: functEntry("4")).place(x=10, y=155)
btn5 = Button(root, text="5", width=3 ,font=('Verdana', 20), command=lambda: functEntry("5")).place(x=80, y=155)
btn6 = Button(root, text="6", width=3 ,font=('Verdana', 20), command=lambda: functEntry("6")).place(x=150, y=155)

btn1 = Button(root, text="1", width=3 ,font=('Verdana', 20), command=lambda: functEntry("1")).place(x=10, y=220)
btn2 = Button(root, text="2", width=3 ,font=('Verdana', 20), command=lambda: functEntry("2")).place(x=80, y=220)
btn3 = Button(root, text="3", width=3 ,font=('Verdana', 20), command=lambda: functEntry("3")).place(x=150, y=220)

btn0 = Button(root, text="0", width=3,font=('Verdana', 20), command=lambda: functEntry("0")).place(x=10, y=285)
btnIgual = Button(root, text="=", width=3 ,font=('Verdana', 20), command=Iqual).place(x=80, y=285)
btnClear = Button(root, text="C", width=3 ,font=('Verdana', 20, "italic"), command=deleteEntry).place(x=150, y=285)

btnDivi = Button(root, text="÷", width=3 ,font=('Verdana', 20, "bold"), command=lambda: oper("÷")).place(x=220, y=90)
btnMulti = Button(root, text="×", width=3 ,font=('Verdana', 20, "bold"), command=lambda: oper("×")).place(x=220, y=155)
btnRest = Button(root, text="-", width=3 ,font=('Verdana', 20, "bold"), command=lambda: oper("-")).place(x=220, y=220)
btnSum = Button(root, text="+", width=3 ,font=('Verdana', 20, "bold"), command=lambda: oper("+")).place(x=220, y=285)

global serialPort
portString = StringVar()
portEntry = Entry(root, bd=0, width=39, textvariable=portString, font=('Verdana', 8), justify='left', state="disable")
portEntry.grid()
portEntry.place(x=10, y=350)
portEntry.config(state="normal")
portEntry.insert(0, "Conectado al puerto: " + serialPort)
portEntry.config(state="disable")
portEntry.config(disabledbackground="#272822", disabledforeground="#fff")

root.mainloop()
