int LEDS[8];
int matrizInt[7];
String val="";
char aux;

void setup() {
  iniciar();
  for(int i=0; i<8; i++){
    pinMode(LEDS[i], OUTPUT);
  }
  Serial.begin(9600);
}

void loop() {
  if(Serial.available()==7){
    val="";
    for (int i=0; i<7; i++) {
        matrizInt[i] = Serial.read();
        aux = char(matrizInt[i]);
        val+=aux;
    }
    delay(100);

    calc();
    
    Serial.println();
  }
}

void calc(){
  char matrizAux[8], matrizChar[8];
  char aux1;
  String var1="", var2="", sig="";
  int result = 0;
  String stringBin;

  val.toCharArray(matrizAux, 8); 

  for(int i=0; i<3; i++){
    var1 += char(matrizAux[i]);
  }
  for(int i=4; i<7; i++){
    var2 += char(matrizAux[i]);
  }
  sig = char(matrizAux[3]);

  if(sig == "+"){
    result = var1.toInt() + var2.toInt();
  }
  if(sig == "-"){
    result = var1.toInt() - var2.toInt();
  }
  if(sig == "x"){
    result = var1.toInt() * var2.toInt();
  }
  if(sig == "/"){
    result = var1.toInt() / var2.toInt();
  }
  Serial.print(var1 + " " + var2 + " " + result);

  stringBin = String(result, BIN);

  Serial.print("\t" + stringBin);
  
  for(int i=0; i<8; i++){
    matrizAux[i] = '0';
  }

  //stringBin.toCharArray(matrizChar, 8);

  printBin(stringBin);
  
}

void printBin(String stringBin){
  int cont = 0;
  char matrizAux[9], matrizChar[9];
  int contAux = 9;

  stringBin.toCharArray(matrizChar, 9);

  //verificamos el tamaño de la matriz
  for(int i=0; i<9; i++){
      if(matrizChar[i] != '\0'){
        cont++;
      }else{
        break;
      }
  }

  Serial.print(cont);
  Serial.print("\t");

  for(int i=0; i<9; i++){
    matrizAux[i] = '0';
  }

  contAux-= cont;

  for(int i=0; i<9; i++){
    if(matrizChar[i] != '\0'){
      matrizAux[contAux] = matrizChar[i];
      contAux++;
    }else{
      break;
    }
  }

  //int aux;
  for(int i=0; i<9; i++){
    if(i<8){
      matrizAux[i] = matrizAux[i+1];
      Serial.print(matrizAux[i]);
    }
  }
  
  int x=0;
  for(int i=0; i<8; i++){
     if(matrizAux[x] == '1'){
        digitalWrite(LEDS[i], HIGH);
     }else{
        digitalWrite(LEDS[i], LOW);
     }
     x++;
  }
}

void iniciar(){
  int x = 6;
  for(int i=0; i<8; i++){
    LEDS[i] = x;
    x++;
  }
}
